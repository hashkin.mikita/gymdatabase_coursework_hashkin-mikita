-- Drop existing tables if they exist
/*
DROP TABLE IF EXISTS Users CASCADE;
DROP TABLE IF EXISTS Memberships CASCADE;
DROP TABLE IF EXISTS Trainers CASCADE;
DROP TABLE IF EXISTS ActivityTypes CASCADE;
DROP TABLE IF EXISTS Rooms CASCADE;
DROP TABLE IF EXISTS Classes CASCADE;
DROP TABLE IF EXISTS ClassBookings CASCADE;
DROP TABLE IF EXISTS Attendance CASCADE;
DROP TABLE IF EXISTS Equipment CASCADE;
DROP TABLE IF EXISTS Payments CASCADE;
DROP TABLE IF EXISTS PaymentMethods CASCADE;
DROP TABLE IF EXISTS EquipmentLogs CASCADE;
*/
-- Table Creation

-- Memberships Table
CREATE TABLE IF NOT EXISTS Memberships (
    ID SERIAL PRIMARY KEY,
    Type VARCHAR(50) NOT NULL,
    Price DECIMAL(10, 2) NOT NULL,
    Duration INT NOT NULL -- Duration in months
);

-- Users Table
CREATE TABLE IF NOT EXISTS Users (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Email VARCHAR(255) NOT NULL UNIQUE,
    Phone VARCHAR(20),
    Address VARCHAR(255),
    MembershipID INT
);

-- Trainers Table
CREATE TABLE IF NOT EXISTS Trainers (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Specialty VARCHAR(255),
    Phone VARCHAR(20),
    Email VARCHAR(255)
);

-- ActivityTypes Table
CREATE TABLE IF NOT EXISTS ActivityTypes (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Description VARCHAR(255)
);

-- Rooms Table
CREATE TABLE IF NOT EXISTS Rooms (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Location VARCHAR(255)
);

-- Classes Table
CREATE TABLE IF NOT EXISTS Classes (
    ID SERIAL PRIMARY KEY,
    ActivityTypeID INT,
    TrainerID INT,
    RoomID INT,
    Price DECIMAL(10, 2)
);

-- ClassBookings Table
CREATE TABLE IF NOT EXISTS ClassBookings (
    ID SERIAL PRIMARY KEY,
    UserID INT,
    ClassID INT,
    BookingDate TIMESTAMP
);

-- Attendance Table
CREATE TABLE IF NOT EXISTS Attendance (
    ID SERIAL PRIMARY KEY,
    UserID INT,
    RoomID INT,
    AttendanceDate TIMESTAMP
);

-- Equipment Table
CREATE TABLE IF NOT EXISTS Equipment (
    ID SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Status VARCHAR(50),
    PurchaseDate TIMESTAMP,
    RoomID INT
);

-- PaymentMethods Table
CREATE TABLE IF NOT EXISTS PaymentMethods (
    ID SERIAL PRIMARY KEY,
    Method VARCHAR(50) NOT NULL
);

-- Payments Table
CREATE TABLE IF NOT EXISTS Payments (
    ID SERIAL PRIMARY KEY,
    UserID INT,
    MembershipID INT,
    ClassID INT,
    Amount DECIMAL(10, 2),
    PaymentMethodID INT,
    PaymentDate TIMESTAMP
);

-- Foreign Key Constraints

-- Users Table
ALTER TABLE Users
ADD CONSTRAINT fk_membership
FOREIGN KEY (MembershipID) REFERENCES Memberships(ID);

-- Classes Table
ALTER TABLE Classes
ADD CONSTRAINT fk_activity_type
FOREIGN KEY (ActivityTypeID) REFERENCES ActivityTypes(ID),
ADD CONSTRAINT fk_trainer
FOREIGN KEY (TrainerID) REFERENCES Trainers(ID),
ADD CONSTRAINT fk_room
FOREIGN KEY (RoomID) REFERENCES Rooms(ID);

-- ClassBookings Table
ALTER TABLE ClassBookings
ADD CONSTRAINT fk_user
FOREIGN KEY (UserID) REFERENCES Users(ID),
ADD CONSTRAINT fk_class
FOREIGN KEY (ClassID) REFERENCES Classes(ID);

-- Attendance Table
ALTER TABLE Attendance
ADD CONSTRAINT fk_attendance_user
FOREIGN KEY (UserID) REFERENCES Users(ID),
ADD CONSTRAINT fk_attendance_room
FOREIGN KEY (RoomID) REFERENCES Rooms(ID);

-- Equipment Table
ALTER TABLE Equipment
ADD CONSTRAINT fk_equipment_room
FOREIGN KEY (RoomID) REFERENCES Rooms(ID);

-- Payments Table
ALTER TABLE Payments
ADD CONSTRAINT fk_payment_user
FOREIGN KEY (UserID) REFERENCES Users(ID),
ADD CONSTRAINT fk_payment_membership
FOREIGN KEY (MembershipID) REFERENCES Memberships(ID),
ADD CONSTRAINT fk_payment_class
FOREIGN KEY (ClassID) REFERENCES Classes(ID),
ADD CONSTRAINT fk_payment_method
FOREIGN KEY (PaymentMethodID) REFERENCES PaymentMethods(ID);

-- Index Creation

CREATE INDEX idx_user_email ON Users (Email);
CREATE INDEX idx_booking_user ON ClassBookings (UserID);
CREATE INDEX idx_attendance_user ON Attendance (UserID);

-- Trigger Creation

-- Equipment Deletion Logging
CREATE TABLE IF NOT EXISTS EquipmentLogs (
    ID SERIAL PRIMARY KEY,
    EquipmentID INT,
    Action VARCHAR(50),
    ActionDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION log_equipment_delete()
RETURNS TRIGGER AS $$
BEGIN
    INSERT INTO EquipmentLogs (EquipmentID, Action)
    VALUES (OLD.ID, 'Deleted');
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER after_equipment_delete
AFTER DELETE ON Equipment
FOR EACH ROW
EXECUTE FUNCTION log_equipment_delete();

-- Procedure Creation

-- Procedure to Add a New User
CREATE OR REPLACE PROCEDURE AddUser (
    p_name VARCHAR,
    p_email VARCHAR,
    p_phone VARCHAR,
    p_address VARCHAR,
    p_membership_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO Users (Name, Email, Phone, Address, MembershipID)
    VALUES (p_name, p_email, p_phone, p_address, p_membership_id);
END;
$$;

-- Procedure to Add a New Trainer
CREATE OR REPLACE PROCEDURE AddTrainer (
    p_name VARCHAR,
    p_specialty VARCHAR,
    p_phone VARCHAR,
    p_email VARCHAR
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO Trainers (Name, Specialty, Phone, Email)
    VALUES (p_name, p_specialty, p_phone, p_email);
END;
$$;

-- Procedure to Add New Equipment
CREATE OR REPLACE PROCEDURE AddEquipment (
    p_name VARCHAR,
    p_status VARCHAR,
    p_purchase_date TIMESTAMP,
    p_room_id INT
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO Equipment (Name, Status, PurchaseDate, RoomID)
    VALUES (p_name, p_status, p_purchase_date, p_room_id);
END;
$$;

-- Procedure to Add a New Class
CREATE OR REPLACE PROCEDURE AddClass (
    p_activity_type_id INT,
    p_trainer_id INT,
    p_room_id INT,
    p_price DECIMAL
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO Classes (ActivityTypeID, TrainerID, RoomID, Price)
    VALUES (p_activity_type_id, p_trainer_id, p_room_id, p_price);
END;
$$;

-- Procedure to Make a Payment for a Class
CREATE OR REPLACE PROCEDURE MakePayment (
    p_user_id INT,
    p_membership_id INT,
    p_class_id INT,
    p_amount DECIMAL,
    p_payment_method_id INT,
    p_payment_date TIMESTAMP
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO Payments (UserID, MembershipID, ClassID, Amount, PaymentMethodID, PaymentDate)
    VALUES (p_user_id, p_membership_id, p_class_id, p_amount, p_payment_method_id, p_payment_date);
END;
$$;

/*
-- Adding initial data for testing

-- Insert into Memberships
INSERT INTO Memberships (Type, Price, Duration) VALUES ('Standard', 50.00, 12);

-- Testing AddUser procedure
CALL AddUser('Jane Doe', 'jane.doe@example.com', '0987654321', '456 Elm St', 1);
-- SELECT * FROM Users WHERE Email = 'jane.doe@example.com';

-- Testing AddTrainer procedure
CALL AddTrainer('Mike Smith', 'Yoga', '555-1234', 'mike.smith@example.com');
-- SELECT * FROM Trainers WHERE Email = 'mike.smith@example.com';

-- Insert into ActivityTypes
INSERT INTO ActivityTypes (Name, Description) VALUES ('Yoga', 'A mind and body practice');
-- SELECT * FROM ActivityTypes WHERE Name = 'Yoga';

-- Insert into Rooms
INSERT INTO Rooms (Name, Location) VALUES ('Room 1', 'First Floor');
-- SELECT * FROM Rooms WHERE Name = 'Room 1';

-- Testing AddEquipment procedure
CALL AddEquipment('Stationary Bike', 'Operational', '2023-05-01', 1);
-- SELECT * FROM Equipment WHERE Name = 'Stationary Bike';

-- Testing AddClass procedure
CALL AddClass(1, 1, 1, 20.00);
-- SELECT * FROM Classes WHERE Price = 20.00;

-- Insert into PaymentMethods
INSERT INTO PaymentMethods (Method) VALUES ('Credit Card');
-- SELECT * FROM PaymentMethods WHERE Method = 'Credit Card';

-- Testing MakePayment procedure
CALL MakePayment(1, 1, 1, 100.00, 1, '2023-06-01');
-- SELECT * FROM Payments WHERE Amount = 100.00;

-- Testing after_equipment_delete trigger
DELETE FROM Equipment WHERE Name = 'Stationary Bike';
-- SELECT * FROM EquipmentLogs WHERE Action = 'Deleted';
*/