# ER Diagram for Gym Management System

## Description
This ER diagram is a database structure for the fitness club Management System (Gym Management System). It includes tables for managing users, memberships, coaches, classes, equipment, and payments.

## Diagram
[View the ER Diagram](https://dbdiagram.io/d/6539269effbf5169f07029a0)

## Chart Code
```sql
Table User {
  ID int [pk, increment]
  Name varchar
  Email varchar
  Phone varchar
  Address varchar
  MembershipID int
}

Table Membership {
  ID int [pk, increment]
  Type varchar
  Price decimal
  Duration int
}

Table Trainer {
  ID int [pk, increment]
  Name varchar
  Specialty varchar
  Phone varchar
  Email varchar
}

Table ActivityType {
  ID int [pk, increment]
  Name varchar
  Description varchar
}

Table Class {
  ID int [pk, increment]
  ActivityTypeID int
  TrainerID int
  RoomID int
  Price decimal
}

Table ClassBooking {
  ID int [pk, increment]
  UserID int
  ClassID int
  BookingDate datetime
}

Table Attendance {
  ID int [pk, increment]
  UserID int
  RoomID int
  AttendanceDate datetime
}

Table Equipment {
  ID int [pk, increment]
  Name varchar
  Status varchar
  PurchaseDate datetime
  RoomID int
}

Table Payment {
  ID int [pk, increment]
  UserID int
  MembershipID int
  ClassID int
  Amount decimal
  PaymentMethodID int
  PaymentDate datetime
}

Table PaymentMethod {
  ID int [pk, increment]
  Method varchar
}

Table Room {
  ID int [pk, increment]
  Name varchar
  Location varchar
}

Ref: User.MembershipID > Membership.ID
Ref: Class.ActivityTypeID > ActivityType.ID
Ref: Class.TrainerID > Trainer.ID
Ref: Class.RoomID > Room.ID
Ref: ClassBooking.UserID > User.ID
Ref: ClassBooking.ClassID > Class.ID
Ref: Attendance.UserID > User.ID
Ref: Attendance.RoomID > Room.ID
Ref: Equipment.RoomID > Room.ID
Ref: Payment.UserID > User.ID
Ref: Payment.MembershipID > Membership.ID
Ref: Payment.ClassID > Class.ID
Ref: Payment.PaymentMethodID > PaymentMethod.ID
