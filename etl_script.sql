-- Drop existing tables if they exist
/*
DROP TABLE IF EXISTS Fact_Sales CASCADE;
DROP TABLE IF EXISTS Fact_Attendance CASCADE;
DROP TABLE IF EXISTS Dim_Date CASCADE;
DROP TABLE IF EXISTS Dim_Customer CASCADE;
DROP TABLE IF EXISTS Dim_Product CASCADE;
DROP TABLE IF EXISTS Dim_Trainer_SCD CASCADE;
DROP TABLE IF EXISTS Dim_Membership_SCD CASCADE;
*/

-- Create dimension tables if they do not exist
CREATE TABLE IF NOT EXISTS Dim_Date (
    DateID SERIAL PRIMARY KEY,
    Date DATE UNIQUE,
    Month INT,
    Year INT
);

CREATE TABLE IF NOT EXISTS Dim_Customer (
    CustomerID SERIAL PRIMARY KEY,
    CustomerName VARCHAR(100) UNIQUE,
    Region VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS Dim_Product (
    ProductID SERIAL PRIMARY KEY,
    ProductName VARCHAR(100),
    Category VARCHAR(50),
    UNIQUE(ProductID)
);

CREATE TABLE IF NOT EXISTS Dim_Trainer_SCD (
    TrainerID SERIAL,
    TrainerName VARCHAR(100),
    Specialty VARCHAR(100),
    Rate DECIMAL(10, 2),
    StartDate DATE,
    EndDate DATE,
    IsCurrent BOOLEAN,
    PRIMARY KEY (TrainerID, StartDate),
    UNIQUE (TrainerID)
);

CREATE TABLE IF NOT EXISTS Dim_Membership_SCD (
    MembershipID SERIAL,
    MembershipType VARCHAR(100),
    Price DECIMAL(10, 2),
    Duration INT,
    StartDate DATE,
    EndDate DATE,
    IsCurrent BOOLEAN,
    PRIMARY KEY (MembershipID, StartDate),
    UNIQUE (MembershipID)
);

-- Fill dimension tables
INSERT INTO Dim_Date (Date, Month, Year)
SELECT DISTINCT DATE(BookingDate) AS Date, EXTRACT(MONTH FROM BookingDate) AS Month, EXTRACT(YEAR FROM BookingDate) AS Year
FROM ClassBookings
ON CONFLICT (Date) DO NOTHING;

INSERT INTO Dim_Customer (CustomerName, Region)
SELECT DISTINCT Name, Address
FROM Users
ON CONFLICT (CustomerName) DO NOTHING;

INSERT INTO Dim_Product (ProductID, ProductName, Category)
SELECT DISTINCT 
    ID AS ProductID, 
    'Class' AS ProductName,
    ActivityTypeID::VARCHAR AS Category
FROM Classes
ON CONFLICT (ProductID) DO NOTHING;

-- Handle Dim_Trainer_SCD for SCD Type 2
-- This part updates the old records to set EndDate and IsCurrent
WITH NewTrainerData AS (
    SELECT 
        t.ID AS TrainerID,
        t.Name AS TrainerName,
        t.Specialty,
        c.Price AS Rate,
        DATE '2024-01-01' AS StartDate, -- Explicit date type
        NULL::DATE AS EndDate, -- Explicit type for NULL
        TRUE AS IsCurrent
    FROM Trainers t
    JOIN Classes c ON t.ID = c.TrainerID
)
UPDATE Dim_Trainer_SCD
SET EndDate = DATE '2024-01-01', IsCurrent = FALSE -- Explicit date type
WHERE TrainerID IN (SELECT TrainerID FROM NewTrainerData)
AND IsCurrent = TRUE
AND EndDate IS NULL
AND Rate != (SELECT Rate FROM NewTrainerData WHERE TrainerID = Dim_Trainer_SCD.TrainerID);

-- Insert new records into Dim_Trainer_SCD
WITH NewTrainerData AS (
    SELECT 
        t.ID AS TrainerID,
        t.Name AS TrainerName,
        t.Specialty,
        c.Price AS Rate,
        DATE '2024-01-01' AS StartDate, -- Explicit date type
        NULL::DATE AS EndDate, -- Explicit type for NULL
        TRUE AS IsCurrent
    FROM Trainers t
    JOIN Classes c ON t.ID = c.TrainerID
)
INSERT INTO Dim_Trainer_SCD (TrainerID, TrainerName, Specialty, Rate, StartDate, EndDate, IsCurrent)
SELECT 
    n.TrainerID,
    n.TrainerName,
    n.Specialty,
    n.Rate,
    n.StartDate,
    n.EndDate,
    n.IsCurrent
FROM NewTrainerData n
LEFT JOIN Dim_Trainer_SCD d ON n.TrainerID = d.TrainerID AND d.IsCurrent = TRUE
WHERE d.TrainerID IS NULL OR (d.Rate != n.Rate AND d.IsCurrent = TRUE)
ON CONFLICT (TrainerID, StartDate) DO NOTHING;

-- Handle Dim_Membership_SCD for SCD Type 2
-- This part updates the old records to set EndDate and IsCurrent
WITH NewMembershipData AS (
    SELECT 
        ID AS MembershipID,
        Type AS MembershipType,
        Price,
        Duration,
        DATE '2024-01-01' AS StartDate, -- Explicit date type
        NULL::DATE AS EndDate, -- Explicit type for NULL
        TRUE AS IsCurrent
    FROM Memberships
)
UPDATE Dim_Membership_SCD
SET EndDate = DATE '2024-01-01', IsCurrent = FALSE -- Explicit date type
WHERE MembershipID IN (SELECT MembershipID FROM NewMembershipData)
AND IsCurrent = TRUE
AND EndDate IS NULL
AND Price != (SELECT Price FROM NewMembershipData WHERE MembershipID = Dim_Membership_SCD.MembershipID);

-- Insert new records into Dim_Membership_SCD
WITH NewMembershipData AS (
    SELECT 
        ID AS MembershipID,
        Type AS MembershipType,
        Price,
        Duration,
        DATE '2024-01-01' AS StartDate, -- Explicit date type
        NULL::DATE AS EndDate, -- Explicit type for NULL
        TRUE AS IsCurrent
    FROM Memberships
)
INSERT INTO Dim_Membership_SCD (MembershipID, MembershipType, Price, Duration, StartDate, EndDate, IsCurrent)
SELECT 
    n.MembershipID,
    n.MembershipType,
    n.Price,
    n.Duration,
    n.StartDate,
    n.EndDate,
    n.IsCurrent
FROM NewMembershipData n
LEFT JOIN Dim_Membership_SCD d ON n.MembershipID = d.MembershipID AND d.IsCurrent = TRUE
WHERE d.MembershipID IS NULL OR (d.Price != n.Price AND d.IsCurrent = TRUE)
ON CONFLICT (MembershipID, StartDate) DO NOTHING;

-- Create fact tables if they do not exist
CREATE TABLE IF NOT EXISTS Fact_Sales (
    SalesID SERIAL PRIMARY KEY,
    DateID INT,
    TrainerID INT,
    MembershipID INT,
    CustomerID INT,
    Quantity INT,
    TotalSales DECIMAL(10, 2),
    FOREIGN KEY (DateID) REFERENCES Dim_Date(DateID),
    FOREIGN KEY (TrainerID) REFERENCES Dim_Trainer_SCD(TrainerID),
    FOREIGN KEY (MembershipID) REFERENCES Dim_Membership_SCD(MembershipID),
    FOREIGN KEY (CustomerID) REFERENCES Dim_Customer(CustomerID)
);

CREATE TABLE IF NOT EXISTS Fact_Attendance (
    AttendanceID SERIAL PRIMARY KEY,
    DateID INT,
    CustomerID INT,
    ClassID INT,
    FOREIGN KEY (DateID) REFERENCES Dim_Date(DateID),
    FOREIGN KEY (CustomerID) REFERENCES Dim_Customer(CustomerID),
    FOREIGN KEY (ClassID) REFERENCES Classes(ID)
);

-- Fill fact tables
INSERT INTO Fact_Sales (DateID, TrainerID, CustomerID, Quantity, TotalSales)
SELECT 
    (SELECT DateID FROM Dim_Date WHERE Date = DATE(p.PaymentDate)) AS DateID,
    (SELECT TrainerID FROM Dim_Trainer_SCD WHERE TrainerID = t.ID AND IsCurrent = TRUE) AS TrainerID,
    p.UserID AS CustomerID,
    1 AS Quantity,
    c.Price AS TotalSales
FROM 
    Payments p
JOIN 
    Classes c ON p.ClassID = c.ID
JOIN 
    Trainers t ON c.TrainerID = t.ID
WHERE 
    p.ClassID IS NOT NULL
ON CONFLICT DO NOTHING;

INSERT INTO Fact_Sales (DateID, MembershipID, CustomerID, Quantity, TotalSales)
SELECT 
    (SELECT DateID FROM Dim_Date WHERE Date = DATE(p.PaymentDate)) AS DateID,
    (SELECT MembershipID FROM Dim_Membership_SCD WHERE MembershipID = m.ID AND IsCurrent = TRUE) AS MembershipID,
    p.UserID AS CustomerID,
    1 AS Quantity,
    m.Price AS TotalSales
FROM 
    Payments p
JOIN 
    Memberships m ON p.MembershipID = m.ID
WHERE 
    p.MembershipID IS NOT NULL
ON CONFLICT DO NOTHING;

INSERT INTO Fact_Attendance (DateID, CustomerID, ClassID)
SELECT 
    (SELECT DateID FROM Dim_Date WHERE Date = DATE(a.AttendanceDate)) AS DateID,
    a.UserID AS CustomerID,
    a.RoomID AS ClassID
FROM 
    Attendance a
ON CONFLICT DO NOTHING;

-- Select data for verification
/*
SELECT * FROM Fact_Sales;
SELECT * FROM Fact_Attendance;
SELECT * FROM Dim_Date;
SELECT * FROM Dim_Trainer_SCD;
SELECT * FROM Dim_Membership_SCD;
SELECT * FROM Dim_Customer;
*/
