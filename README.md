### Documentation

#### ERD.md
- **Purpose**: Contains the Entity-Relationship Diagram (ERD) for the database schema.
- **Usage**: Refer to this file to understand the relationships between different tables in the database.

#### db_oltp_schema.sql
- **Purpose**: Defines the schema for the OLTP database.
- **Usage**: Run this script to create the necessary tables for the OLTP database.
- **Execution**: Open pgAdmin, connect to your database, and execute this script to create the tables.
- **Content Overview**:
  - **Table Definitions**: Contains SQL commands to create tables such as `Users`, `Classes`, `Trainers`, etc.

#### data1.csv and data2.csv
- **Purpose**: Provide sample data for loading into the OLTP database.
- **Usage**: Ensure these files are correctly referenced in `script_to_load_data.sql` when loading data.
- **Important Note**: Update the file paths in the `script_to_load_data.sql` script to match the location of your CSV files.

#### script_to_load_data.sql
- **Purpose**: Contains SQL commands to load data from CSV files into the OLTP database.
- **Usage**: Run this script after creating the OLTP schema to populate the database with initial data.
- **Execution**:
  1. **Open the script**.
  2. **Update file paths**:
     ```sql
     COPY temp_data(table_name, ID, Type, Price, Duration, Name, Email, Phone, Address, MembershipID, Specialty, Status, PurchaseDate, RoomID, ActivityTypeID, TrainerID, ClassPrice, UserID, ClassID, BookingDate, AttendanceDate, Amount, PaymentMethodID, PaymentDate)
     FROM 'C:\\path\\to\\your\\data1.csv' DELIMITER ',' CSV HEADER;

     COPY temp_data(table_name, ID, Type, Price, Duration, Name, Email, Phone, Address, MembershipID, Specialty, Status, PurchaseDate, RoomID, ActivityTypeID, TrainerID, ClassPrice, UserID, ClassID, BookingDate, AttendanceDate, Amount, PaymentMethodID, PaymentDate)
     FROM 'C:\\path\\to\\your\\data2.csv' DELIMITER ',' CSV HEADER;
     ```
  3. **Execute the script** in pgAdmin to load data from the provided CSV files into the database.
- **Content Overview**:
  - **Temporary Table**: Creates a temporary table to load data.
  - **Data Loading**: Uses `COPY` commands to load data from CSV files.
  - **Data Insertion**: Inserts data from the temporary table into the main tables.

#### etl_script.sql
- **Purpose**: This script performs the ETL (Extract, Transform, Load) process, moving data from the OLTP database to the OLAP database.
- **Usage**: Run this script to create dimension and fact tables in the OLAP database and populate them with data from the OLTP database.
- **Execution**: Execute this script in pgAdmin to set up the OLAP tables and insert the necessary data.
- **Content Overview**:
  - **Drop Tables**: Drops existing OLAP tables if they exist.
  - **Create Dimension Tables**: Creates tables like `Dim_Date`, `Dim_Customer`, `Dim_Product`, `Dim_Trainer_SCD`, and `Dim_Membership_SCD`.
  - **Create Fact Tables**: Creates tables like `Fact_Sales` and `Fact_Attendance`.
  - **Insert Data**: Inserts data into the dimension and fact tables.
  - **Update Mechanism for SCD Type 2**:
    - **`Dim_Trainer_SCD` Update**:
      ```sql
      WITH NewTrainerData AS (
          SELECT 
              t.ID AS TrainerID,
              t.Name AS TrainerName,
              t.Specialty,
              c.Price AS Rate,
              DATE '2024-01-01' AS StartDate,
              NULL::DATE AS EndDate,
              TRUE AS IsCurrent
          FROM Trainers t
          JOIN Classes c ON t.ID = c.TrainerID
      )
      UPDATE Dim_Trainer_SCD
      SET EndDate = DATE '2024-01-01', IsCurrent = FALSE
      WHERE TrainerID IN (SELECT TrainerID FROM NewTrainerData)
      AND IsCurrent = TRUE
      AND EndDate IS NULL
      AND Rate != (SELECT Rate FROM NewTrainerData WHERE TrainerID = Dim_Trainer_SCD.TrainerID);
      ```
      - **Explanation**: Updates the old records to set `EndDate` and `IsCurrent` when the rate changes.
    - **`Dim_Membership_SCD` Update**:
      ```sql
      WITH NewMembershipData AS (
          SELECT 
              ID AS MembershipID,
              Type AS MembershipType,
              Price,
              Duration,
              DATE '2024-01-01' AS StartDate,
              NULL::DATE AS EndDate,
              TRUE AS IsCurrent
          FROM Memberships
      )
      UPDATE Dim_Membership_SCD
      SET EndDate = DATE '2024-01-01', IsCurrent = FALSE
      WHERE MembershipID IN (SELECT MembershipID FROM NewMembershipData)
      AND IsCurrent = TRUE
      AND EndDate IS NULL
      AND Price != (SELECT Price FROM NewMembershipData WHERE MembershipID = Dim_Membership_SCD.MembershipID);
      ```
      - **Explanation**: Updates the old records to set `EndDate` and `IsCurrent` when the price changes.
  - **Insert New Records**:
    - **New records are inserted into `Dim_Trainer_SCD` and `Dim_Membership_SCD` if they do not exist or if the rate/price changes**.

#### Gym Data Analysis Report.pbix
- **Purpose**: Contains the Power BI report for visualizing the gym data.
- **Usage**: Open this file in Power BI Desktop to view the pre-built visualizations based on the loaded data.
- **Content Overview**:
  - **Pre-built Visualizations**: Includes various charts and tables to analyze gym data.
  - **Interaction**: You can filter and drill down into the data to explore different aspects of the gym's performance.

This documentation provides a basic guide to setting up and running the project.
