-- Load data from CSV files into the database

-- Create temporary table for loading data
CREATE TEMP TABLE temp_data (
    table_name VARCHAR(50),
    ID INT,
    Type VARCHAR(50),
    Price DECIMAL(10, 2),
    Duration INT,
    Name VARCHAR(255),
    Email VARCHAR(255),
    Phone VARCHAR(20),
    Address VARCHAR(255),
    MembershipID INT,
    Specialty VARCHAR(255),
    Status VARCHAR(50),
    PurchaseDate DATE,
    RoomID INT,
    ActivityTypeID INT,
    TrainerID INT,
    ClassPrice DECIMAL(10, 2),
    UserID INT,
    ClassID INT,
    BookingDate TIMESTAMP,
    AttendanceDate TIMESTAMP,
    Amount DECIMAL(10, 2),
    PaymentMethodID INT,
    PaymentDate TIMESTAMP
);

-- Load data into the temporary table from data1.csv
COPY temp_data(table_name, ID, Type, Price, Duration, Name, Email, Phone, Address, MembershipID, Specialty, Status, PurchaseDate, RoomID, ActivityTypeID, TrainerID, ClassPrice, UserID, ClassID, BookingDate, AttendanceDate, Amount, PaymentMethodID, PaymentDate)
FROM 'C:\\gymdatabase_coursework\\data1.csv'
DELIMITER ','
CSV HEADER;

-- Load data into the temporary table from data2.csv
COPY temp_data(table_name, ID, Type, Price, Duration, Name, Email, Phone, Address, MembershipID, Specialty, Status, PurchaseDate, RoomID, ActivityTypeID, TrainerID, ClassPrice, UserID, ClassID, BookingDate, AttendanceDate, Amount, PaymentMethodID, PaymentDate)
FROM 'C:\\gymdatabase_coursework\\data2.csv'
DELIMITER ','
CSV HEADER;

-- Insert data from the temporary table into the appropriate main tables

-- Memberships table
INSERT INTO Memberships (ID, Type, Price, Duration)
SELECT ID, Type, Price, Duration
FROM temp_data
WHERE table_name = 'Memberships'
ON CONFLICT (ID) DO NOTHING;
select * from Memberships

-- ActivityTypes table
INSERT INTO ActivityTypes (ID, Name, Description)
SELECT ID, Type AS Name, Specialty AS Description
FROM temp_data
WHERE table_name = 'ActivityTypes'
ON CONFLICT (ID) DO NOTHING;
select * from ActivityTypes

-- Rooms table
INSERT INTO Rooms (ID, Name, Location)
SELECT ID, Type AS Name, Address AS Location
FROM temp_data
WHERE table_name = 'Rooms'
ON CONFLICT (ID) DO NOTHING;
select * from Rooms

-- Trainers table
INSERT INTO Trainers (ID, Name, Specialty, Phone, Email)
SELECT ID, Name, Specialty, Phone, Email
FROM temp_data
WHERE table_name = 'Trainers'
ON CONFLICT (ID) DO NOTHING;
select * from Trainers

-- PaymentMethods table
INSERT INTO PaymentMethods (ID, Method)
SELECT ID, Name AS Method
FROM temp_data
WHERE table_name = 'PaymentMethods'
ON CONFLICT (ID) DO NOTHING;
select * from PaymentMethods

-- Users table
INSERT INTO Users (ID, Name, Email, Phone, Address, MembershipID)
SELECT ID, Name, Email, Phone, Address, MembershipID
FROM temp_data
WHERE table_name = 'Users'
ON CONFLICT (ID) DO NOTHING;
select * from Users

-- Equipment table
INSERT INTO Equipment (ID, Name, Status, PurchaseDate, RoomID)
SELECT ID, Name, Status, PurchaseDate, RoomID
FROM temp_data
WHERE table_name = 'Equipment'
ON CONFLICT (ID) DO NOTHING;
select * from Equipment

-- Classes table
INSERT INTO Classes (ID, ActivityTypeID, TrainerID, RoomID, Price)
SELECT ID, ActivityTypeID, TrainerID, RoomID, ClassPrice
FROM temp_data
WHERE table_name = 'Classes'
ON CONFLICT (ID) DO NOTHING;
select * from Classes

-- ClassBookings table
INSERT INTO ClassBookings (ID, UserID, ClassID, BookingDate)
SELECT ID, UserID, ClassID, BookingDate
FROM temp_data
WHERE table_name = 'ClassBookings'
ON CONFLICT (ID) DO NOTHING;
select * from ClassBookings

-- Attendance table
INSERT INTO Attendance (ID, UserID, RoomID, AttendanceDate)
SELECT ID, UserID, RoomID, AttendanceDate
FROM temp_data
WHERE table_name = 'Attendance'
ON CONFLICT (ID) DO NOTHING;
select * from Attendance

-- Payments table
INSERT INTO Payments (ID, UserID, MembershipID, ClassID, Amount, PaymentMethodID, PaymentDate)
SELECT ID, UserID, MembershipID, ClassID, Amount, PaymentMethodID, PaymentDate
FROM temp_data
WHERE table_name = 'Payments'
ON CONFLICT (ID) DO NOTHING;
select * from Payments

-- Drop the temporary table
DROP TABLE temp_data;